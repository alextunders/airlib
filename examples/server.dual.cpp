#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../Socket.hpp"
#include <errno.h>
#include <unistd.h>

SocketServer server;

int downloadStream(struct Socket *client){
    printf("New client (ds): %p\n", client);
    try{
        while(client->port & SOCKET_STATE_ALIVE){
                socketSend(client, (char*)"Test of something", strlen("Test of something"));
                sleep(5);
        }
    }catch(struct SocketException e){
        fprintf(stderr, "caller: %i:%p, errno: %s\n\tdesc: %s\n", e.caller, e.where, strerror(errno), e.desc);
    }
    server.deleteClient(client);
    return 0;
}
int uploadStream(struct Socket *client){
    printf("New client (us): %p\n", client);
    char *getted = (char*)client->buffer;
    try{
        while(client->port & SOCKET_STATE_ALIVE){
            getted = socketRead(client);
            if(!strcmp(getted, "\x11 End of transmission \x1f"))
                break;
            printf("Recivied from client: %s\n", getted);
        }
    }catch(struct SocketException e){
        fprintf(stderr, "caller: %i:%p, errno: %s\n\tdesc: %s\n", e.caller, e.where, strerror(errno), e.desc);
    }
    server.deleteClient(client);
    return 0;
}

int main(int argc, char **argv){
    socketSetExceptionHandler([](int caller, struct Socket *where, char *desc){
		throw ((struct SocketException){caller, where, desc});
		return;
	});
    server.bind("127.0.0.1", atoi(argv[1]));
    server.setListener(SOCKET_DOWNLOAD_LISTENER, (int(*)(void*))downloadStream);
    server.setListener(SOCKET_UPLOADS_LISTENER, (int(*)(void*))uploadStream);
    server.start();
    char *input = (char*)malloc(512);
    while(1){
        memset(input, 0x0, 512);
        for(int i = 0;;i++){
            char c = getchar();
            if(c == '\n')
                break;
            else
                input[i] = c;
        }
        if(!strcmp(input, "stat")){
            printf("Clients: %i\n=========\n", server.getClientCount());
            for(int i = 0; i < server.getClientCount(); i++){
                printf("\t%i - %p (%i)\n", i, server.getClient(i), server.getClient(i)->fd);
            }
        }else if(!strncmp(input, "delete", 7)){
            server.deleteClient(atoi(input+7));
        }
    }
}