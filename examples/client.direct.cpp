#include <stdio.h>
#include <stdlib.h>
#include "../Socket.hpp"
#include <errno.h> 
#include <unistd.h>
#include <string.h>

SocketClient client;

int main(int argc, char **argv){
    socketSetExceptionHandler([](int caller, struct Socket *where, char *desc){
		throw ((struct SocketException){caller, where, desc});
		return;
	});
    client.connect("127.0.0.1", atoi(argv[1]));
    for(int i = 0; i < 5; i++){
        client.send("Hello from client!", 19);
        printf("From server: %s\n", client.read());
    }
    client.close();
    return 0;
}