#include <stdio.h>
#include <stdlib.h>
#include "../Socket.hpp"
#include <errno.h> 
#include <unistd.h>
#include <string.h>

SocketClient client;

int echo(struct Socket *c){
    try{
        for(int i = 0; i < 15; i++){
            socketSend(c, "Test from client", 17);
            printf("From server: %s\n", socketRead(c));
            sleep(1);
        }
    }catch(SocketException e){
        fprintf(stderr, "caller: %i:%p, errno: %s\n\tdesc: %s\n", e.caller, e.where, strerror(errno), e.desc);
    }
    return 0;
}

int main(int argc, char **argv){
    socketSetExceptionHandler([](int caller, struct Socket *where, char *desc){
		throw ((struct SocketException){caller, where, desc});
		return;
	});
    client.connect("127.0.0.1", atoi(argv[1]));
    client.setListener(SOCKET_DEFAULT_LISTENER, echo);
    client.start();
    while(client.state() & SOCKET_STATE_ALIVE);
    return 0;
}