#ifndef SOCKET_OSSA_H
#define SOCKET_OSSA_H
#include <pthread.h>
#ifdef __unix__
    #define socket_t int
    #include <netinet/in.h>
#endif
#ifdef __WIN32
    #include <windows.h>
#endif
//Socket states
#define SOCKET_STATE_ALIVE             1<<0 //Connection is alive
#define SOCKET_STATE_ECHO              1<<1 //Echo mode or dual listeners
#define SOCKET_STATE_REBINDABLE        1<<2 //Did it enable to rebind or reconnect?
#define SOCKET_STATE_UPSTREAMA         1<<3 // Did upstream is alive?
#define SOCKET_STATE_DOWNSTREAMA       1<<4 // Did downstream is alive?
#define SOCKET_STATE_UPDATE_REQ        1<<5 // Request for updating socket info (full)
//Socket Update requests
#define SOCKET_UPDATE_DELETE           -1 // Request for deleting client
#define SOCKET_UPDATE_RESTART          -2
//Listeners
#define SOCKET_DEFAULT_LISTENER        -1 //Echo   Network Thread
#define SOCKET_UPLOADS_LISTENER        0 //First  Network Thread
#define SOCKET_DOWNLOAD_LISTENER       1 //Second Network Thread
//Codes of functions
#define SOCKET_FUNC_UNKNOWEN 0
#define SOCKET_FUNC_INIT 1
#define SOCKET_FUNC_SEND 2
#define SOCKET_FUNC_READ 3
#define SOCKET_FUNC_BIND 4
#define SOCKET_FUNC_CONN 5
#define SOCKET_FUNC_KILL 6
#define SOCKET_FUNC_LSTN 7
#define SOCKET_FUNC_ACPT 8
struct Socket{
    socket_t fd;
    int bufflen, port;
    struct sockaddr_in address;
    void *buffer; char *addr;
};
struct SocketListener{
    int type;
    pthread_t ptmain;
    int (**stream)(void *raw);
};

extern void socketSetExceptionHandler(void(*h)(int caller, struct Socket *where, char *desc));
extern void socketExceptionCall(int caller, struct Socket *where, char *desc);
extern struct Socket initSocket(int bufflen);
extern struct Socket createSocket(char *addr, int port, int bufflen);
extern int socketSend(struct Socket*, char *data, int len);
extern char *socketRead(struct Socket*);
extern int socketClose(struct Socket*);
extern int socketKill(struct Socket*);
extern int socketClient(struct Socket*);
extern int socketConnect(struct Socket*, char *addr, int port);
extern int socketServer(struct Socket*);
extern int socketBind(struct Socket*, char *addr, int port);
extern int socketListen(struct Socket*, int cc);
struct Socket *socketAccept(struct Socket *from);
#endif