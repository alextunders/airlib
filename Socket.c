/* clbs.build.head
build.clbs.exec: shell
build.format: build@target.system
build@object.unix: gcc -c $src -o $out
build@shared.unix: gcc -c $src -shared -fPIC -o $out
clbs.build.end */
#include "./Socket.h"

#define SOCKET_VERSION "190222"
#define SOCKET_OSSA_C

#include <dlist/list.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#ifdef __unix__
    #include <unistd.h>
    #include <sys/socket.h>
    #include <sys/types.h>
    #include <sys/ioctl.h>
    #include <net/if.h>
    #include <arpa/inet.h>
    #include "./Socket.unix.c"
#elif defined(__WIN32)
    #include "./Socket.win32.c"
#endif