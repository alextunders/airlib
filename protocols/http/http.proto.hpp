extern "C"{
    #include <dlist/list.h>
}

const char *strMethods[] = {
    "GET", "HEAD", "POST", "PUT", "DELETE", "CONNECT", "OPTIONS",
    "TRACE", "PATCH", "RESPONSE"
};

#define HTTP_GET (void*)(strMethods[0])
#define HTTP_HEAD (void*)(strMethods[1])
#define HTTP_POST (void*)(strMethods[2])
#define HTTP_PUT (void*)(strMethods[3])
#define HTTP_DELETE (void*)(strMethods[4])
#define HTTP_CONNECT (void*)(strMethods[5])
#define HTTP_OPTIONS (void*)(strMethods[6])
#define HTTP_TRACE (void*)(strMethods[7])
#define HTTP_PATH (void*)(strMethods[8])
#define HTTP_RESPONSE (void*)(strMethods[9])

/*
For more comfortable work here uses virtual fields
here is only 2 virtual fields:
    @version - version of HTTP
    @file - file for request or path to file if it's response
    @code - response code (example - 404/200/304)
    @desc - response description (examlpe - OK, Not Found, Access Limited)
    @content - content of request
*/
class HTTPHeader{
    protected:
        int size = 2048;
        struct __list entry;
        char *content, *httpbuffer;
    public:
        HTTPHeader(int bsize = 2048);
        HTTPHeader(char *src, int bsize = 2048);
        ~HTTPHeader();

        void* type;
        void set(const char *entry);
        void set(const char *fieldname, const char *data);
        char *get(const char *fieldname);
        char *flush();
        void fill(char *src);
};