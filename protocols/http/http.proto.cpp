#include "./http.proto.hpp"
#include <sstream>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

HTTPHeader::HTTPHeader(int bsize){
    this->content = 0x0;
    this->entry = makeEmptyList();
    this->httpbuffer = (char*)malloc(bsize);
}
HTTPHeader::HTTPHeader(char *src, int bsize){
    this->fill(src);
    this->httpbuffer = (char*)malloc(bsize);
}
void HTTPHeader::set(const char *entry){
    listAppend(&this->entry, (void*)entry, strlen(entry));
}
void HTTPHeader::set(const char *fieldname, const char *data){
    int newlen = strlen(fieldname) + strlen(data) + 2;
    if(!strncmp(fieldname, "@content", 9)){
        this->set("@content-type: allocated");
        if(this->content != 0x0)
            free(this->content);
        this->content = (char*)malloc(strlen(data));
        strcpy(this->content, data);
        return;
    }
    for(int i = 0; i < listLen(&this->entry); i++){
        if(!strncmp(fieldname, (char*)listGet(&this->entry, i), strlen(fieldname))){
            struct __list *f = listFrame(&this->entry, i);
            if(f->data != 0x0)
                free(f->data);
            f->data = (struct __list *)malloc(newlen<=16?32:newlen);
            sprintf((char*)f->data, "%s: %s", fieldname, data);
            return;
        }
    }
    char *tmp = (char*)malloc(newlen);
    sprintf(tmp, "%s: %s", fieldname, data);
    int index = listAppendLink(&this->entry, tmp);
    return;
}
char *HTTPHeader::get(const char *fieldname){
    for(int i = 0; i < listLen(&this->entry); i++){
        if(!strncmp(fieldname, (char*)listGet(&this->entry, i), strlen(fieldname))){
            return (char*)((listGet(&this->entry, i)+2 + strlen(fieldname)));
        }
    }
    return 0x0;
}
char *HTTPHeader::flush(){
    free(this->httpbuffer);
    this->httpbuffer = (char*)malloc(this->size);
    memset(this->httpbuffer, 0x0, this->size);
    if(this->type == HTTP_RESPONSE){
        sprintf(this->httpbuffer, "%s %s %s", this->get("@version"), this->get("@code"), this->get("@desc"));
    }else{
        sprintf(this->httpbuffer, "%s %s %s", this->get("@version"), (char*)this->type, this->get("@file"));
    }
    for(struct __list *l = &this->entry; l!=0x0; l = l->next){
        if(l->data == 0x0 || *(char*)(l->data)=='\0') continue;
        else if(*(char*)l->data == '@'|| *(char*)(l->data)==' ' || *(char*)(l->data)=='\n') continue;
        else sprintf(this->httpbuffer, "%s\n%s", this->httpbuffer, (char*)l->data);
    }
    sprintf(this->httpbuffer, "%s\n\n%s", this->httpbuffer, this->content==0x0?"\0":this->content);
    return this->httpbuffer;
}
void readWord(char *src, char *to, int *counter, char sep = ' '){
    if(src[*counter] == sep) *counter++;
    for(int i = *counter; i < strlen(src); i++){
        if(src[i] == sep){
            *counter = i;
            break;
        }
        to[i-(*counter)] = src[i];
    }
}
void HTTPHeader::fill(char *src){
    free(this->httpbuffer);
    this->httpbuffer = (char*)malloc(strlen(src));
    memcpy(this->httpbuffer, src, strlen(src));
    int c = 0;
    char line[512];
    { //Methodline
        readWord(src, line, &c, '\n');
        char version[12], word[12], word2[256];
        sscanf(line, "%s %s %s", version, word, word2);
        this->set("@version", version);
        if(*word < 58) /*This is response*/ {
            this->type = HTTP_RESPONSE;
            this->set("@code", word);
            this->set("@desc", word2);
        }else /*This is request*/ {
            for(int i = 0; i < 10; i++){
                if(!strcmp(word, strMethods[i])){
                    this->type = (void*)(strMethods[i]);
                    break;
                } else continue;
            }
            this->set("@file", word2);
        }
    }
    { //Entries
        for(int i = 0; ; i++){
            memset(line, 0x0, 512);
            int f = 0;
            readWord(src+c+1, line, &f, '\n');
            c += strlen(line)+1;
            if(line[0] == 0x0) break;
            else this->set(line);
        }
    }
    free(this->content);
    content = (char*)malloc(strlen(src) - c);
    strcpy(content, src+c);
}
HTTPHeader::~HTTPHeader(){
    free(this->content);
    free(this->httpbuffer);
    earaseList(&this->entry);
}