#include "./Socket.hpp"
extern "C"{
	#include <dlist/list.h>
	#include <netinet/in.h>
	#include <pthread.h>
	#include <string.h>
	#include <stdio.h>
	#include <stdlib.h>
}

void init() { //Init of lib (setting up handler)
	socketSetExceptionHandler((void(*)(int, struct Socket*, char *desc))([](int caller, struct Socket *where, char *desc){
		throw ((struct SocketException){caller, where, desc});
		return;
	}));
}
void fini() { //Finishing and cleaning

}

struct serverListenerAttributes{
	int maxClients;
	SocketServer *server;
	SocketListener *listener;
	struct __list *threads;
};

void *serverProccess(void *serverPTR){
	//TODO: разделить на отдельную проверку состояния сокетов и отдельно "слухавку"
	struct serverListenerAttributes *sla = (struct serverListenerAttributes*)(serverPTR);
	while(sla->server->state() & (SOCKET_STATE_ALIVE)){
		if(sla->server->getClientCount() < sla->maxClients==-1?__INT_MAX__:sla->maxClients){
			struct Socket *client = sla->server->listen(); // it's adding client automatiiclly
			client->port = SOCKET_STATE_ALIVE | SOCKET_STATE_DOWNSTREAMA | SOCKET_STATE_UPSTREAMA;
			pthread_t pid[2];
			pthread_create(&pid[0], 0x0, (void*(*)(void*))(sla->listener->stream[0]), client);
			listAppend(sla->threads, pid, sizeof(pthread_t));
			if(!(sla->server->state() & (SOCKET_STATE_ECHO))){
				pthread_create(&pid[1], 0x0, (void*(*)(void*))(sla->listener->stream[1]), client);
				listAppend(&sla->threads[1], &pid[1], sizeof(pthread_t));
				pthread_detach(pid[1]);
			}
			pthread_detach(pid[0]);
		}else{
			continue;
		}
	}
	return 0x0;
}
void *serverStater(void *serverPTR){
	struct serverListenerAttributes *sla = (struct serverListenerAttributes*)(serverPTR);
	pthread_t process;
	pthread_create(&process, 0x0, serverProccess, serverPTR);
	pthread_detach(process);
	while(sla->server->state() & (SOCKET_STATE_ALIVE)){
		if(sla->server->state() & (SOCKET_STATE_UPDATE_REQ)){
			for(int i = 0; i < sla->server->getClientCount(); i++){
				struct Socket *c = sla->server->getClient(i);
				if(!(c->port & (SOCKET_STATE_ALIVE))){
					//Request for deleting
					pthread_t tid[2];
					tid[0] = *(pthread_t*)listGet(&sla->threads[0], i);
					listRemove(&sla->threads[0], i);
					if(c->port & SOCKET_STATE_UPSTREAMA)
						pthread_cancel(tid[0]);
					if(!(sla->server->state() & SOCKET_STATE_ECHO)){
						tid[1] = *(pthread_t*)listGet(&sla->threads[1], i);
						if(c->port & SOCKET_STATE_DOWNSTREAMA)
							pthread_cancel(tid[1]);
						listRemove(&sla->threads[1], i);
					}
					sla->server->deleteClient(i);
				}
			}
			// OK, This is bad practice, so DO NOT DO NEXT!
			*(int*)(sla->server) &= ~(SOCKET_STATE_UPDATE_REQ);
		}
	}
	pthread_cancel(process);
	return 0;
}

SocketProto::SocketProto(int bufflen){
    this->sock = initSocket(bufflen);
	this->sock.bufflen = bufflen;
	this->listeners.stream = (int(**)(void*))malloc(sizeof(int(*)(void*))*2);
}
int SocketProto::setListener(int type, int (*listener)(void *)){
	if(type>SOCKET_DOWNLOAD_LISTENER||type<SOCKET_DEFAULT_LISTENER){
		socketExceptionCall(SOCKET_FUNC_SETL, &this->sock, (char*)listener);
	}
	if(type == SOCKET_DEFAULT_LISTENER)
    		{this->listeners.stream[0] = listener;this->flag|=(SOCKET_STATE_ECHO);}
	else
    		{this->listeners.stream[type] = listener;this->flag&=~(SOCKET_STATE_ECHO);}
    return 0;
}
int SocketProto::setBufflen(int bufflen){
    if(bufflen <= 0){
		
    }
    this->sock.bufflen = bufflen;
    free(this->sock.buffer);
    this->sock.buffer = malloc(bufflen);
    return 0;
}
int SocketProto::setTimeout(long long _us){
    this->timeout = _us;
    return 0;
}
int SocketProto::kill(){
    return socketKill(&this->sock);
}
int SocketProto::close(){
    return socketClose(&this->sock);
}
SocketServer::SocketServer(){
	this->connectionList = makeEmptyList();
	this->threadList[0] = makeEmptyList();
	this->threadList[1] = makeEmptyList();
}
SocketServer::SocketServer(const char* addr, int port, int bufflen){
    this->bind(addr, port, bufflen);
}
int SocketServer::bind(const char *addr, int port, int bufflen){
    this->setBufflen(bufflen);
    socketBind(&this->sock, (char*)addr, port);
    return htons(port);
}
int SocketServer::getClientCount(){
    return listLen(&this->connectionList);
}
struct Socket *SocketServer::getClient(int index){
    return (struct Socket*)listGet(&this->connectionList, index);
}
struct Socket *SocketServer::getClient(const char *addr){
    //Seraching by addr. Isn't ready yet
    return 0x0;
}
int SocketServer::start(int maxClients){
	struct serverListenerAttributes *sla = (struct serverListenerAttributes *)malloc(sizeof(struct serverListenerAttributes));
	sla->listener = &this->listeners;
	sla->threads = this->threadList;
	sla->server = this;
	sla->maxClients = maxClients;
	this->flag |= SOCKET_STATE_ALIVE;
	pthread_create(&this->listeners.ptmain, 0x0, serverStater, sla);
	pthread_detach(this->listeners.ptmain);
	socketListen(&this->sock, this->maxClient);
	return 0;
}
struct Socket *SocketServer::listen(){
	socketListen(&this->sock, 1);
	struct Socket *client = socketAccept(&this->sock);
	listAppendLink(&this->connectionList, client);
	return client;
}
int SocketServer::deleteClient(int cid){
	//So, if we can call it from child thread, so we should not to 
	// delete right now, but send signal to handler to kill threads and socket
	int i = cid;
	if(listGet(&this->threadList[0], i) == 0x0 || listGet(&this->threadList[0], i) == 0x0){
		goto gotoSocketKill1;
	} else if(pthread_self() != *(pthread_t*)listGet(&this->threadList[0], i) \
	&& pthread_self() != *(pthread_t*)listGet(&this->threadList[1], i)){
gotoSocketKill1:
		socketKill(this->getClient(i));
		struct __list *f = listFrame(&this->connectionList, i);
		free(f->data);
		if(f->next == 0x0){
			f->data = 0x0;
		}else{
			f->data = f->next->data;
			f->next = f->next->next;
		}
		socketListen(&this->sock, this->maxClient);
	}
	else{
		this->getClient(i)->port &= ~(SOCKET_STATE_ALIVE);
		this->flag |= (SOCKET_STATE_UPDATE_REQ);
	}
	return 0;
}
int SocketServer::deleteClient(struct Socket *client){
	for(int i = 0; i < this->getClientCount(); i++){
		//So, if we can call it from child thread, so we should not to 
		// delete right now, but send signal to handler to kill threads and socket
		if(this->getClient(i) == client){
			if(listGet(&this->threadList[0], i) == 0x0 || listGet(&this->threadList[0], i) == 0x0){
				goto gotoSocketKill2;
			} else if(pthread_self() != *(pthread_t*)listGet(&this->threadList[0], i) \
			&& pthread_self() != *(pthread_t*)listGet(&this->threadList[1], i)){
gotoSocketKill2:
				socketKill(client);
				struct __list *f = listFrame(&this->connectionList, i);
				free(f->data);
				if(f->next == 0x0){
					f->data = 0x0;
				}else{
					f->data = f->next->data;
					f->next = f->next->next;
				}
				socketListen(&this->sock, this->maxClient);
			}
			else{
				client->port &= ~(SOCKET_STATE_ALIVE);
				this->flag |= (SOCKET_STATE_UPDATE_REQ);
			}
			break;
		}
	}
	return 0;
}
SocketServer::~SocketServer(){
	flag &= ~(SOCKET_STATE_ALIVE);
	// usleep(1500); //While everything will stopped
	earaseList(&this->connectionList);
	earaseList(&this->threadList[0]);
	earaseList(&this->threadList[1]);
	this->kill();
}
int SocketProto::state(){
	return this->flag;
}
int SocketClient::connect(const char* addr, int port, int buffsize){
	if(!(this->flag & SOCKET_STATE_REBINDABLE) && this->flag & SOCKET_STATE_ALIVE){ //if not rebindabe and is already busy -> ignore
		socketExceptionCall(SOCKET_FUNC_CONN, &this->sock, (char*)"flag:rebind is disabled and socket is already in use");
	}else{
		if(this->flag & SOCKET_STATE_ALIVE){
			socketClose(&this->sock);
			int bl = this->sock.bufflen;
			socketKill(&this->sock);
			this->sock = initSocket(bl);
		}
		socketConnect(&this->sock, (char*)addr, port);
	}
	return 0;
}
int SocketClient::send(char *data, int datalen){
	return socketSend(&this->sock, data, datalen);
}
char *SocketClient::read(){
	return socketRead(&this->sock);
}
SocketClient::SocketClient(){

}
SocketClient::~SocketClient(){
	flag &= ~(SOCKET_STATE_ALIVE);
	// usleep(1500); //While everything will stopped
	this->kill();
}
void *clientStarter(void *clientPTR){
	pthread_t s[2];
	SocketClient *client = (SocketClient*)clientPTR;
	struct SocketListener *listeners = (struct SocketListener *)(client+64);
	pthread_create(&s[0], 0x0, (void*(*)(void*))(listeners->stream[0]), client+16);
	pthread_detach(s[0]);
	if(!(client->state() & (SOCKET_STATE_ECHO))){
		pthread_create(&s[1], 0x0, (void*(*)(void*))(listeners->stream[1]), client+16);
		pthread_detach(s[1]);
	}
	while(client->state() & (SOCKET_STATE_ALIVE)){

	}
	pthread_cancel(s[0]);
	if(!(client->state() & (SOCKET_STATE_ECHO)))
		pthread_cancel(s[1]);
	client->kill();
	return 0x0;
}
int SocketClient::close(){
	this->flag &= ~(SOCKET_STATE_ALIVE);
	return 0;
}
int SocketClient::start(){
	pthread_create(&this->listeners.ptmain, 0x0, clientStarter, this);
	pthread_detach(this->listeners.ptmain);
	return 0;
}