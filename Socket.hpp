/* clbs.build
build.exec: shell
build.format: build@target.system
build@deps: "Socket.c"
*/
extern "C"{
	#include "./Socket.h"
	#include <dlist/list.h>
}

#define SOCKET_FUNC_SETB 9
#define SOCKET_FUNC_SETL 10
#define SOCKET_FUNC_MPT	 11
#define SOCKET_SSGTC_DMCC 128

class SocketProto;
class SocketClient;
class SocketServer;

extern char *socketGetExceptionStr(int code);


class SocketProto{
	protected:
		int flag;
		long timeout;
		struct Socket sock;
		struct SocketListener listeners;
	public:
		SocketProto(int bufflen=1024);

		int setListener(int type, int (*listener)(void*));
		int setBufflen(int bufflen);
		int setTimeout(long long _us); // -1 if unlimited
		int state();
		int close();
		int kill();
};
class SocketServer : public SocketProto{
	protected:
		struct __list connectionList, threadList[2];
		int maxClient = SOCKET_SSGTC_DMCC;
	public:
		SocketServer();
		~SocketServer();
		SocketServer(const char* addr, int port, int bufflen=1024);
		int bind(const char* addr, int port, int bufflen=1024);
		int getClientCount();
		int getCID(int fd);
		int getCID(struct Socket *client);
		int deleteClient(int index);
		int deleteClient(struct Socket *client);
		struct Socket *getClient(int index);
		struct Socket *getClient(const char *addr);
		struct Socket *listen(); //Waiting for client
		int start(int clientCount=-1); // -1 if unlimited. Starting listening in background
};
class SocketClient : public SocketProto{
	public:
		SocketClient(struct Socket *sock);
		SocketClient();
		~SocketClient();
		SocketClient(const char *addr, int port, int bufflen=1024);
		int connect(const char *addr, int port, int bufflen=1024);
		int start();
		int close();
		char *read(); //in calling outside of listeners reading CURRENT buffer
		int send(char *data, int datalen);
};
struct SocketException{
	int caller;
	struct Socket *where;
	char *desc;
};