/* clbs.build
build.clbs.exec: shell
build.format: build@target.system
 */
#ifndef SOCKET_OSSA_C
 #include "./Socket.c"
#endif

void (*exceptionHandler)(int caller, struct Socket *where, char *desc) = 0x0;

void socketExceptionCall(int caller, struct Socket *where, char *desc){
    exceptionHandler(caller, where, desc);
}
void socketSetExceptionHandler(void(*h)(int caller, struct Socket *where, char *desc)){
    exceptionHandler = h;
}

struct Socket initSocket(int bufflen){
    struct Socket me;
    me.fd = socket(AF_INET , SOCK_STREAM , 0);
    me.bufflen = bufflen;
    me.buffer = malloc(bufflen);
    return me;
}
struct Socket createSocket(char *addr, int port, int bufflen){
    struct Socket me = initSocket(bufflen);
    if(me.fd == -1) return me;
    me.addr = addr;
    me.port = port;
    me.address.sin_addr.s_addr = inet_addr(addr);
    me.address.sin_family = AF_INET;
    me.address.sin_port = htons(port);
    return me;
}
int socketSend(struct Socket *sock, char *data, int len){
    if(len == -1) len = strlen(data);
    int ret = send(sock->fd, data, len, 0);
    if(ret <= 0)
        exceptionHandler(SOCKET_FUNC_SEND, sock, data);
}
char *socketRead(struct Socket *sock){
    int a = recv(sock->fd, sock->buffer, sock->bufflen, 0);
    if(a<=0)
        exceptionHandler(SOCKET_FUNC_READ, sock, 0x0);
    else
        return (char*)(sock->buffer);
    return 0x0;
}
int socketClose(struct Socket *sock){
    close(sock->fd);
    return 0;
}
int socketKill(struct Socket *sock){
    socketClose(sock);
    free(sock->buffer);
    if(sock->addr != 0x0)
        free(sock->addr);
    return 0;
}
int socketClient(struct Socket* sock){
    return socketConnect(sock, sock->addr, sock->port);
}
int socketConnect(struct Socket* sock, char *addr, int port){
    if(sock->fd == -1) return -1;
    sock->addr = (char*)malloc(strlen(addr));
    strcpy(sock->addr, addr);
    sock->port = port;
    sock->address.sin_addr.s_addr = inet_addr(addr);
    sock->address.sin_family = AF_INET;
    sock->address.sin_port = htons(port);
    int ret = connect(sock->fd, (const struct sockaddr*)&sock->address, sizeof(sock->address));
    if(ret < 0)
        exceptionHandler(SOCKET_FUNC_CONN, sock, addr);
    else return ret;
}
int socketServer(struct Socket* sock){
    return socketBind(sock, sock->addr, sock->port);
}
int socketBind(struct Socket* sock, char *addr, int port){
    if(sock->fd == -1) return -1;
    sock->addr = (char*)malloc(strlen(addr));
    strcpy(sock->addr, addr);
    sock->port = port;
    sock->address.sin_addr.s_addr = inet_addr(addr);
    sock->address.sin_family = AF_INET;
    sock->address.sin_port = htons(port);
    int ret = bind(sock->fd, (const struct sockaddr*)&sock->address, sizeof(sock->address));
    if(ret < 0)
        exceptionHandler(SOCKET_FUNC_BIND, sock, addr);
    else return ret;
}
int socketListen(struct Socket*s, int cc){
    int ret = listen(s->fd, cc);
    if(ret < 0)
        exceptionHandler(SOCKET_FUNC_LSTN, s, 0x0);
    else return ret;
}
struct Socket *socketAccept(struct Socket *from){
    struct Socket *new = malloc(sizeof(struct Socket));
    int c = sizeof(struct sockaddr_in);
    new->fd = accept(from->fd, (struct sockaddr *)&new->address, &c);
    if(new->fd < 0)
        exceptionHandler(SOCKET_FUNC_ACPT, new, 0x0);
    new->bufflen = from->bufflen;
    new->buffer=malloc(from->bufflen);
    return new;
}